#include <stdio.h>
#include <stdlib.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int info;
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

int eh_no_vermelho(ArvoreRB * no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

int buscar (ArvoreRB *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}



ArvoreRB* novo_no(int info, int cor) {
    ArvoreRB *a = (ArvoreRB*) malloc(sizeof(ArvoreRB));
    a->info = info;
    a->esq = a->dir = NULL;
    a->cor = RED;
    return a;
}

ArvoreRB* direcao_dir(ArvoreRB* dir) {
    ArvoreRB *esq = dir->esq;
    dir->esq = esq->dir;
    esq->dir = dir;
    esq->cor = dir->cor;
    dir->cor = RED;
    return esq;
}

ArvoreRB* direcao_esq(ArvoreRB* esq) {
    ArvoreRB *dir = esq->dir;
    esq->dir = dir->esq;
    dir->esq = esq;
    dir->cor = esq->cor;
    esq->cor = RED;
    return dir;
}

void inverte_cor (ArvoreRB *a) {
    a->cor = !(a->cor);
    a->esq->cor = !(a->esq->cor);
    a->dir->cor = !(a->dir->cor);
}

ArvoreRB* insere_ArvoreRB (ArvoreRB *a, int info) {
    if (a == NULL) return novo_no(info, 0);
    if (info < a->info)
        a->esq = insere_ArvoreRB(a->esq, info);
    else if (info > a->info)
        a->dir = insere_ArvoreRB(a->dir, info);
    else
        return a;
    if (eh_no_vermelho(a->dir) && !eh_no_vermelho(a->esq)) {
        a = direcao_esq(a);
    }
    else if (eh_no_vermelho(a->esq) && eh_no_vermelho(a->esq->esq)) {
        a = direcao_dir(a);
    } else if (eh_no_vermelho(a->esq) && eh_no_vermelho(a->dir)) {
        inverte_cor(a);
    }
    return a;

}
void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

void print(ArvoreRB * a, int spaces) {
  if (a == NULL)
      return;
  int i;
  spaces += 10;
  print(a->dir, spaces);
  for(i=10;i<spaces;i++) printf(" ");
  if(a->cor) printf(" ");
  printf("%d\n", a->info);
  print(a->esq, spaces);
}

int main(){
  ArvoreRB * a = NULL;

    a = insere_ArvoreRB(a,50);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,30);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,90);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,20);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,40);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,95);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,10);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,35);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,45);
    a->cor = BLACK;
    a = insere_ArvoreRB(a,37);
    a->cor = BLACK;

    print(a, 0);
}
















